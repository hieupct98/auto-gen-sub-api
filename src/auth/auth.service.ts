import {
  ConflictException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare } from 'bcrypt';
import { PrismaService } from '../prisma.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly jwtService: JwtService,
  ) {}

  public async login(email: string, password: string) {
    const user = await this.prismaService.user.findUnique({
      where: { email: email },
    });

    if (!user || !(await compare(password, user.password))) {
      throw new UnauthorizedException('Invalid credentials. Please try again.');
    }

    if (user.currentJwt) {
      try {
        await this.jwtService.verifyAsync(user.currentJwt);
        throw new ConflictException('Only login from 1 device at a time');
      } catch (e) {
        if (e.name !== 'TokenExpiredError') {
          throw e;
        }
      }
    }

    const token = await this.jwtService.signAsync({
      id: user.id,
      email: user.email,
    });

    await this.prismaService.user.update({
      where: { email: email },
      data: {
        currentJwt: token,
      },
    });

    return {
      email,
      token,
    };
  }

  public async checkToken(headers: any) {
    const authHeader: string = headers.authorization;
    if (!authHeader) {
      throw new UnauthorizedException('Missing Authorization Header');
    }

    if (authHeader.indexOf('Bearer') !== 0 || authHeader.length <= 6) {
      throw new UnauthorizedException(
        `Invalid token. Bad Authorization header. Expected value 'Bearer <JWT>'.`,
      );
    }

    const token = authHeader.replace('Bearer ', '');

    const payload = await this.jwtService.verifyAsync(token);

    const user = await this.prismaService.user.findUnique({
      where: { id: payload.id },
    });

    if (token !== user.currentJwt) {
      throw new UnauthorizedException('Invalid token. Please try again.');
    }

    const newToken = await this.jwtService.signAsync({
      id: payload.id,
      email: payload.email,
    });

    await this.prismaService.user.update({
      where: { id: payload.id },
      data: {
        currentJwt: newToken,
      },
    });

    return {
      token: newToken,
    };
  }
}
